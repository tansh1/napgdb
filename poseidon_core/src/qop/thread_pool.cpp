/*
 * Copyright (C) 2019-2020 DBIS Group - TU Ilmenau, All Rights Reserved.
 *
 * This file is part of the Poseidon package.
 *
 * Poseidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Poseidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Poseidon. If not, see <http://www.gnu.org/licenses/>.
 */

#include "thread_pool.hpp"
#include <iostream>
#include "spdlog/spdlog.h"

#include <pthread.h>

void thread_pool::worker_thread() {
  while (!done) {
    function_wrapper task;
    // try to get the next task from the queue
    if (work_queue.try_pop(task)) {
      // and execute it
      task();
    } else {
      // give another thread the chance to put some work on the queue
      std::this_thread::yield();
    }
  }
}

thread_pool::thread_pool(size_t thread_count) : done(false), joiner(threads) {
  spdlog::debug("Creating thead_pool with {} threads", thread_count);
  try {
    // create a number of worker threads
    for (auto i = 0u; i < thread_count; ++i) {
      threads.push_back(std::thread(&thread_pool::worker_thread, this));
    }
  } catch (...) {
    done = true;
    throw;
  }
}

/**
 * Destructor.
 */
thread_pool::~thread_pool() { done = true; }

/**
 * cpu_thread_pool
 */
void cpu_thread_pool::worker_thread() {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(cpu_core,&cpuset);
    CPU_SET(cpu_core+1,&cpuset);
    CPU_SET(cpu_core+2,&cpuset);
    CPU_SET(cpu_core+3,&cpuset);
     if (pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) ==0) {
         // std::cout << "The thread can bind cpu " << cpu_core << std::endl;
     }

  while (!done) {
    cpu_function_wrapper task;
    // try to get the next task from the queue
    if (work_queue.try_pop(task)) {
      // and execute it
      task();
    } else {
      // give another thread the chance to put some work on the queue
      std::this_thread::yield();
    }
  }
}

void cpu_thread_pool::start(){
  
  try {
    // create a number of worker threads
    for (auto i = 0u; i < thread_count; ++i) {
      threads.push_back(std::thread(&cpu_thread_pool::worker_thread, this));
    }
  } catch (...) {
    done = true;
    throw;
  }

}

cpu_thread_pool::cpu_thread_pool() : done(false), joiner(threads) {
  cpu_core = 0;
}

cpu_thread_pool::cpu_thread_pool(int my_cpu_core) : done(false), joiner(threads),cpu_core(my_cpu_core) {
}

/**
 * Destructor.
 */
cpu_thread_pool::~cpu_thread_pool() { done = true; }