function get_node_id(node) 
	return id(node) 
end

function produce_something()
	return 42
end

function test_func_1(actor)
	return node_property(actor, "name")
end

function test_func_2()
	return "func1", "(sig)"
end