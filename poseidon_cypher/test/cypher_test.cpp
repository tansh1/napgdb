#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do
                          // this in one cpp file

#include "catch.hpp"
#include "defs.hpp"
#include "graph_db.hpp"
#include "query_compiler.hpp"

// #include "lua_poseidon.hpp"

#ifdef USE_PMDK
namespace nvm = pmem::obj;

#define PMEMOBJ_POOL_SIZE ((size_t)(1024 * 1024 * 80))

nvm::pool_base prepare_pool() {
  auto pop = nvm::pool_base::create("/mnt/pmem0/poseidon/cypher_test", "",
                                    PMEMOBJ_POOL_SIZE);
  return pop;
}
#endif

graph_db_ptr create_graph(
#ifdef USE_PMDK
    nvm::pool_base &pop
#endif
) {
#ifdef USE_PMDK
  graph_db_ptr graph;
  nvm::transaction::run(pop, [&] { graph = p_make_ptr<graph_db>(); });
#else
  auto graph = p_make_ptr<graph_db>();
#endif

#ifdef USE_TX
  auto tx = graph->begin_transaction();
#endif

  auto a1 = graph->add_node(
      "Actor", {{"name", boost::any(std::string("Leonardo DiCaprio"))}});
  auto a2 = graph->add_node(
      "Actor", {{"name", boost::any(std::string("Ken Watanabe"))}});
  auto a3 = graph->add_node(
      "Actor", {{"name", boost::any(std::string("Matthew McConaughey"))}});
  auto m1 = graph->add_node("Movie",
                            {{"title", boost::any(std::string("Inception"))}});
  auto m2 = graph->add_node(
      "Movie", {{"title", boost::any(std::string("Interstellar"))}});

  graph->add_relationship(a1, m1, "PLAYED_IN", {});
  graph->add_relationship(a2, m1, "PLAYED_IN", {});
  graph->add_relationship(a3, m2, "PLAYED_IN", {});

#ifdef USE_TX
  graph->commit_transaction();
#endif

  return graph;
}

TEST_CASE("Testing the cypher parser", "[cypher]") {
#ifdef USE_PMDK
  auto pop = prepare_pool();
  auto graph = create_graph(pop);
#else
  auto graph = create_graph();
#endif

  SECTION("Query #1: Results") {
#ifdef USE_TX
    auto tx = graph->begin_transaction();
#endif

    query_compiler qc(graph);

    result_set rs;
    auto qp =
        qc.parse("MATCH (p)-[:PLAYED_IN]->(m:Movie { title: \"Inception\" }) "
                 "RETURN p, m");
    REQUIRE(qp != nullptr);

    spdlog::info("transform ast to plan...");
    auto plan = qc.ast_to_plan(qp, &rs);
    plan.dump();
    std::cout << std::endl;
    spdlog::info("execute plan...");
    plan.start();

    rs.wait();
    spdlog::info("rs.size = {}", rs.data.size());
    result_set expected;

    expected.append({query_result("Actor[0]{name: \"Leonardo DiCaprio\"}"),
                     query_result("Movie[3]{title: \"Inception\"}")});
    expected.append({query_result("Actor[1]{name: \"Ken Watanabe\"}"),
                     query_result("Movie[3]{title: \"Inception\"}")});

    REQUIRE(rs == expected);
    std::cout << rs;

#ifdef USE_TX
    graph->abort_transaction();
#endif
  }

  SECTION("Query #2: Results") {
#ifdef USE_TX
    auto tx = graph->begin_transaction();
#endif

    query_compiler qc(graph);

    result_set rs;
    auto qp = qc.parse("MATCH (m:Movie) RETURN m.title");
    REQUIRE(qp != nullptr);

    spdlog::info("transform ast to plan...");
    auto plan = qc.ast_to_plan(qp, &rs);
    plan.dump();
    std::cout << std::endl;
    spdlog::info("execute plan...");
    plan.start();

    rs.wait();
    result_set expected;

    expected.append({query_result("Inception")});
    expected.append({query_result("Interstellar")});

    REQUIRE(rs == expected);
    std::cout << rs;

#ifdef USE_TX
    graph->abort_transaction();
#endif
  }
#ifdef USE_PMDK
  nvm::transaction::run(pop, [&] { nvm::delete_persistent<graph_db>(graph); });
  pop.close();
  remove("/mnt/pmem0/poseidon/cypher_test");
#endif
}

TEST_CASE("Testing the error handling of the cypher parser", "[cypher]") {
#ifdef USE_PMDK
  auto pop = prepare_pool();
  auto graph = create_graph(pop);
#else
  auto graph = create_graph();
#endif

#ifdef USE_TX
  auto tx = graph->begin_transaction();
#endif

  query_compiler qc(graph);

  REQUIRE(qc.parse("MATCH (p)->(o)") == nullptr);
  REQUIRE(qc.parse("SET x, y") == nullptr);
  REQUIRE(qc.parse("MATCH (p)-[:PLAYED_IN]->(o)") == nullptr);
  REQUIRE(qc.parse("MATCH a, b, c") == nullptr);
  REQUIRE(qc.parse("MATCH a, b, c RETURN *") == nullptr);

#ifdef USE_PMDK
  nvm::transaction::run(pop, [&] { nvm::delete_persistent<graph_db>(graph); });
  pop.close();
  remove("/mnt/pmem0/poseidon/cypher_test");
#endif
}

TEST_CASE("Testing the query compiler", "[cypher]") {
#ifdef USE_PMDK
  auto pop = prepare_pool();
  auto graph = create_graph(pop);
#else
  auto graph = create_graph();
#endif

#ifdef USE_TX
  auto tx = graph->begin_transaction();
#endif

  query_compiler qcomp(graph);

  SECTION("basic node search") {
    std::string q("MATCH (p:Person { name: \"Bart Simpson\"}) RETURN p");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Person])=>is_property([name])=>project([ 0+ ])=>"
            "printer()");
  }

  SECTION("simple node-relationship-node pattern with labels") {
    std::string q("MATCH (a:Actor)-[:PLAYED_IN]->(m:Movie) RETURN *");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Actor])=>foreach_from_relationship([PLAYED_IN])"
            "=>get_to_node()=>node_has_label([Movie])=>printer()");
  }

  SECTION("simple node-relationship-node pattern with labels and property") {
    std::string q("MATCH (a:Actor {name: \"Leonardo "
                  "DiCaprio\"})-[:PLAYED_IN]->(m:Movie) RETURN *");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Actor])=>is_property([name])=>foreach_from_"
            "relationship([PLAYED_IN])"
            "=>get_to_node()=>node_has_label([Movie])=>printer()");
  }

  SECTION("simple node-relationship-node pattern without second labels") {
    std::string q("MATCH (a:Actor)-[:PLAYED_IN]->(m) RETURN a, m");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Actor])=>foreach_from_relationship([PLAYED_IN])"
            "=>get_to_node()=>project([ 0+ 2+ ])=>printer()");
  }

  SECTION("simple node-relationship-node pattern with property") {
    std::string q("MATCH (a)-[:PLAYED_IN]->(m:Movie {title: \"Inception "
                  "(2010)\"}) RETURN a");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([])=>foreach_from_relationship([PLAYED_IN])=>"
            "get_to_node()=>node_has_label([Movie])=>is_property([title])=>"
            "project([ 0+ ])=>printer()");
  }

  SECTION("simple rl node-relationship-node pattern with property") {
    std::string q("MATCH (m:Movie {title: \"Inception "
                  "(2010)\"})<-[:PLAYED_IN]-(a) RETURN a.name");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() == "scan_nodes([Movie])=>is_property([title])=>"
                        "foreach_to_relationship([PLAYED_IN])=>"
                        "get_from_node()=>project([ 2+ ])=>printer()");
  }

  SECTION("recursive rl node-relationship-node pattern") {
    std::string q("MATCH (p:Person)-[:HAS_FRIENDS*1..5]->(o) RETURN p, o.name");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Person])=>foreach_variable_from_relationship([HAS_"
            "FRIENDS, (1,5)])=>get_to_node()=>project([ 0+ 2+ ])=>printer()");
  }

  SECTION("creating a simple node") {
    std::string q("CREATE (p:Person)");
    auto ast = qcomp.parse(q);

    auto plan = qcomp.ast_to_plan(ast);
    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() == "create_node([Person])");
  }

  SECTION("creating a complete node") {
    std::string q("CREATE (p:Person { name: \"John\", age: 42 })");
    auto ast = qcomp.parse(q);
    REQUIRE(ast != nullptr);

    auto plan = qcomp.ast_to_plan(ast);
    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() == "create_node([Person], {age: 42, name: \"John\"})");
  }

  SECTION("creating a relationship node") {
    spdlog::info("creating a relationship node");
    std::string q(
        "MATCH (a1:Actor { name: \"Leonardo DiCaprio\" }), (a2:Actor { name: "
        "\"Ken Watanabe\" }) CREATE (a1)-[:KNOWS]->(a2) RETURN *");
    auto ast = qcomp.parse(q);
    REQUIRE(ast != nullptr);

    auto plan = qcomp.ast_to_plan(ast);
    std::ostringstream os;
    plan.dump(os);
    spdlog::info("plan = {}", os.str());
    // REQUIRE(os.str() == "scan_nodes([Actor])=>is_property()=>, {age: 42,
    // name: \"John\"})");
  }

  SECTION("updating a node") {
    std::string q(
        "MATCH (p:Person) SET p.name = \"John\", p.age = 41 RETURN p");
    auto ast = qcomp.parse(q);

    auto plan = qcomp.ast_to_plan(ast);
    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Person])=>update_node([ 0 ], {age: 41, name: "
            "\"John\"})=>project([ 0+ ])=>printer()");
  }

  SECTION("updating a node with properties") {
    std::string q(
        "MATCH (p:Person) SET p = {name: \"John\", age: 42} RETURN p");
    auto ast = qcomp.parse(q);

    auto plan = qcomp.ast_to_plan(ast);
    std::ostringstream os;
    plan.dump(os);
    REQUIRE(os.str() ==
            "scan_nodes([Person])=>update_node([ 0 ], {age: 42, name: "
            "\"John\"})=>project([ 0+ ])=>printer()");
  }

/*
  lua_interp::set_udf_library("../lua/poseidon.lua");

  SECTION("query with CALL operator for procedure") {
    spdlog::info("CALL #1");
    std::string q("MATCH (a:Actor) CALL test_func_1(a) YIELD res RETURN res");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);

    REQUIRE(os.str() == "scan_nodes([Actor])=>call_lua_procedure(test_func_1[ "
                        "0 ])=>project([ 1 ])=>printer()");
  }

  SECTION("query with CALL operator") {
    spdlog::info("CALL #2");
    std::string q("CALL test_func_2() YIELD name, signature RETURN signature");
    auto ast = qcomp.parse(q);
    auto plan = qcomp.ast_to_plan(ast);

    std::ostringstream os;
    plan.dump(os);

    REQUIRE(os.str() ==
            "call_lua_procedure(test_func_2[ ])=>project([ 1 ])=>printer()");
  }
*/
#ifdef USE_PMDK
  nvm::transaction::run(pop, [&] { nvm::delete_persistent<graph_db>(graph); });
  pop.close();
  remove("/mnt/pmem0/poseidon/cypher_test");
#endif
}
