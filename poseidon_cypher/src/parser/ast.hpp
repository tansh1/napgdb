#ifndef ast_hpp_
#define ast_hpp_

#include "properties.hpp"
#include <boost/variant.hpp>
#include <memory>
#include <vector>

namespace ast {

class qnode;
using qnode_ptr = std::shared_ptr<qnode>;
using qnode_list = std::vector<qnode_ptr>;

enum class qnode_type {
  nt_match,
  nt_unwind,
  nt_call,
  nt_return,
  nt_create,
  nt_set,
  nt_delete,
  nt_root
};

class qnode {
protected:
  qnode() = default;

public:
  virtual ~qnode() = default;
  virtual void print(int level = 0) = 0;
  virtual qnode_type type() const = 0;
};

struct node_pattern {
  std::string variable;
  std::string label;
  properties_t props;
};

struct relationship_pattern {
  enum direction { undefined, left_to_right, right_to_left, both, none };
  direction dir;
  std::string variable;
  std::string label;
  std::pair<int, int> range;
  properties_t props;

  relationship_pattern(direction d) : dir(d) {}
  relationship_pattern(direction d, const std::string &v, const std::string &l,
                       std::pair<int, int> r, const properties_t &p)
      : dir(d), variable(v), label(l), range(r), props(p) {}
};

using pattern = boost::variant<node_pattern, relationship_pattern>;
using plist = std::vector<pattern>;
using plist_set = std::vector<plist>;

class match_node : public qnode {
public:
  match_node() = default;
  match_node(const plist &pl) : pattern_list_(pl) {}

  void print(int level = 0) override;
  qnode_type type() const override { return qnode_type::nt_match; }
  plist pattern_list_;
};

struct return_item {
  // TODO: expr as expression tree!
  std::string expr;
  std::string variable;
};

class return_node : public qnode {
public:
  return_node() = default;
  return_node(const std::vector<return_item> &ri) : ritems_(ri) {}
  void print(int level = 0) override;
  qnode_type type() const override { return qnode_type::nt_return; }

  std::vector<return_item> ritems_;
  qnode_ptr order_;
  qnode_ptr skip_;
  qnode_ptr limit_;
};

class call_node : public qnode {
public:
  call_node() = default;
  call_node(const std::string &pname, const std::vector<return_item> &av)
      : proc_name_(pname), args_(av) {}
  void print(int level = 0) override;
  qnode_type type() const override { return qnode_type::nt_call; }

  void yield_items(const std::vector<std::string> &yitems) { yitems_ = yitems; }

  std::string proc_name_;
  std::vector<return_item> args_;
  std::vector<std::string> yitems_;
};

class update_node : public qnode {
public:
  using pattern = boost::variant<node_pattern, relationship_pattern>;
  update_node() = default;
  update_node(qnode_type n, const std::vector<pattern> &pl)
      : ntype_(n), pattern_list_(pl) {}

  void print(int level = 0) override;
  qnode_type type() const override { return ntype_; }

  qnode_type ntype_;
  std::vector<pattern> pattern_list_;
};

class query_node : public qnode {
public:
  query_node() = default;

  void print(int level = 0) override;
  qnode_type type() const override { return qnode_type::nt_root; }

  std::vector<qnode_ptr> update_nodes_;
  std::vector<qnode_ptr> reading_nodes_;
  std::shared_ptr<return_node> return_node_;
};

using query_ptr = std::shared_ptr<query_node>;

} // namespace ast

std::ostream &operator<<(std::ostream &os, const ast::node_pattern &n);
std::ostream &operator<<(std::ostream &os, const ast::relationship_pattern &r);

#endif
