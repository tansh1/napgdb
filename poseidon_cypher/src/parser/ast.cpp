#include "ast.hpp"
#include <boost/hana.hpp>

using namespace ast;

std::ostream &operator<<(std::ostream &os, const node_pattern &n) {
  os << "node_pattern( " << n.variable << ":" << n.label << "{}"
     << ")";
  return os;
}

std::ostream &operator<<(std::ostream &os, const relationship_pattern &r) {
  os << "rship_pattern( " << r.variable << ":" << r.label << "["
     << r.range.first << "," << r.range.second << "], {}"
     << ")";
  return os;
}

void match_node::print(int level) {
  for (auto i = 0; i < level; i++)
    std::cout << "   ";

  std::cout << "match( ";
  auto my_visitor =
      boost::hana::overload([&](node_pattern &n) { std::cout << n; },
                            [&](relationship_pattern &r) { std::cout << r; });
  for (auto &p : pattern_list_) {
    boost::apply_visitor(my_visitor, p);
    std::cout << " ";
  }

  std::cout << ")" << std::endl;
}

void return_node::print(int level) {
  for (auto i = 0; i < level; i++)
    std::cout << "   ";
  std::cout << "return( ";
  for (auto &ri : ritems_) {
    std::cout << ri.expr << " ";
  }
  std::cout << ")" << std::endl;
}

void update_node::print(int level) {
  for (auto i = 0; i < level; i++)
    std::cout << "   ";
  switch (ntype_) {
  case qnode_type::nt_create:
    std::cout << "create( ";
    break;
  case qnode_type::nt_set:
    std::cout << "set( ";
    break;
  default:
    std::cout << "UNKNOWN( ";
    break;
  }
  auto my_visitor =
      boost::hana::overload([&](node_pattern &n) { std::cout << n; },
                            [&](relationship_pattern &r) { std::cout << r; });
  for (auto &p : pattern_list_) {
    boost::apply_visitor(my_visitor, p);
    std::cout << " ";
  }
  std::cout << ")" << std::endl;
}

void call_node::print(int level) {
  for (auto i = 0; i < level; i++)
    std::cout << "   ";
  std::cout << "call( " << proc_name_ << ", [ ";
  for (auto &a : args_) {
    std::cout << a.expr << " ";
  }
  std::cout << "] => [ ";
  for (auto &yi : yitems_) {
    std::cout << yi << " ";
  }
  std::cout << "] )" << std::endl;
}

void query_node::print(int level) {
  for (auto i = 0; i < level; i++)
    std::cout << "   ";
  std::cout << "query(\n";

  for (auto n : reading_nodes_) {
    n->print(level + 1);
  }
  for (auto n : update_nodes_) {
    n->print(level + 1);
  }

  if (return_node_)
    return_node_->print(level + 1);

  for (auto i = 0; i < level; i++)
    std::cout << "   ";
  std::cout << ")" << std::endl;
}
