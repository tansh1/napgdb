#include "cypher_visitor.hpp"
#include "properties.hpp"
#include "spdlog/spdlog.h"
#include <boost/algorithm/string.hpp>

using namespace parser;

cypher_visitor::cypher_visitor() {}

antlrcpp::Any
cypher_visitor::visitOC_Cypher(CypherParser::OC_CypherContext *ctx) {
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_StatementContext *>(child)) {
      return visitOC_Statement(
          dynamic_cast<CypherParser::OC_StatementContext *>(child));
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any
cypher_visitor::visitOC_Statement(CypherParser::OC_StatementContext *ctx) {
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_QueryContext *>(child)) {
      return visitOC_Query(
          dynamic_cast<CypherParser::OC_QueryContext *>(child));
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any
cypher_visitor::visitOC_Query(CypherParser::OC_QueryContext *ctx) {
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_RegularQueryContext *>(child)) {
      return visitOC_RegularQuery(
          dynamic_cast<CypherParser::OC_RegularQueryContext *>(child));
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any cypher_visitor::visitOC_RegularQuery(
    CypherParser::OC_RegularQueryContext *ctx) {
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_SingleQueryContext *>(child)) {
      return visitOC_SingleQuery(
          dynamic_cast<CypherParser::OC_SingleQueryContext *>(child));
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any
cypher_visitor::visitOC_SingleQuery(CypherParser::OC_SingleQueryContext *ctx) {
  // spdlog::info("SingleQuery");
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_SinglePartQueryContext *>(child)) {
      auto c = visitOC_SinglePartQuery(
          dynamic_cast<CypherParser::OC_SinglePartQueryContext *>(child));
      return c.as<std::shared_ptr<ast::query_node>>();
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any cypher_visitor::visitOC_SinglePartQuery(
    CypherParser::OC_SinglePartQueryContext *ctx) {
  // spdlog::info("SinglePartQuery");
  auto qn = std::make_shared<ast::query_node>();
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_ReadingClauseContext *>(child)) {
      auto c = visitOC_ReadingClause(
          dynamic_cast<CypherParser::OC_ReadingClauseContext *>(child));
      auto &nlist = c.as<ast::qnode_list>();
      for (auto n : nlist) {
        qn->reading_nodes_.push_back(n);
      }
    } else if (antlrcpp::is<CypherParser::OC_ReturnContext *>(child)) {
      auto c =
          visitOC_Return(dynamic_cast<CypherParser::OC_ReturnContext *>(child));
      qn->return_node_ = c.as<std::shared_ptr<ast::return_node>>();
    } else if (antlrcpp::is<CypherParser::OC_InQueryCallContext *>(child)) {
    } else if (antlrcpp::is<CypherParser::OC_UpdatingClauseContext *>(child)) {
      auto c = visitOC_UpdatingClause(
          dynamic_cast<CypherParser::OC_UpdatingClauseContext *>(child));
      auto n = c.as<std::shared_ptr<ast::update_node>>();
      qn->update_nodes_.push_back(n);
    }
  }
  return qn;
}

antlrcpp::Any cypher_visitor::visitOC_ReadingClause(
    CypherParser::OC_ReadingClauseContext *ctx) {
  // spdlog::info("ReadingClause");
  ast::qnode_list qnodes;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_MatchContext *>(child)) {
      auto c =
          visitOC_Match(dynamic_cast<CypherParser::OC_MatchContext *>(child));
      auto &nvec = c.as<ast::qnode_list>();
      for (auto n : nvec) {
        qnodes.push_back(n);
      }
    } else if (antlrcpp::is<CypherParser::OC_UnwindContext *>(child)) {
    } else if (antlrcpp::is<CypherParser::OC_InQueryCallContext *>(child)) {
      auto c = visitOC_InQueryCall(
          dynamic_cast<CypherParser::OC_InQueryCallContext *>(child));
      qnodes.push_back(c.as<std::shared_ptr<ast::call_node>>());
    }
  }
  return qnodes;
}

antlrcpp::Any cypher_visitor::visitOC_UpdatingClause(
    CypherParser::OC_UpdatingClauseContext *ctx) {
  // spdlog::info("UpdatingClause");
  return visitChildren(ctx);
}

antlrcpp::Any
cypher_visitor::visitOC_Create(CypherParser::OC_CreateContext *ctx) {
  // spdlog::info("Create");
  ast::plist pattern_list;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_PatternContext *>(child)) {
      auto pattern = visitOC_Pattern(
          dynamic_cast<CypherParser::OC_PatternContext *>(child));
      // TODO: handle all pattern_lists!!
      pattern_list = pattern.as<ast::plist_set>().front();
    }
  }
  return std::make_shared<ast::update_node>(ast::qnode_type::nt_create,
                                            pattern_list);
}

void cypher_visitor::add_to_properties(
    std::map<std::string, properties_t> &pmap, const std::string &pkey,
    boost::any &val) {
  std::vector<std::string> v;
  boost::split(v, pkey, boost::is_any_of("."));
  auto iter = pmap.find(v[0]);
  if (iter == pmap.end()) {
    properties_t props;
    props.insert({v[1], val});
    pmap.insert({v[0], props});
  } else {
    iter->second.insert({v[1], val});
  }
}

antlrcpp::Any cypher_visitor::visitOC_Set(CypherParser::OC_SetContext *ctx) {
  // spdlog::info("Set");
  std::vector<ast::pattern> pattern_list;
  std::map<std::string, properties_t>
      pmap; // a map of variables with corresponding properties

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_SetItemContext *>(child)) {
      auto p = visitOC_SetItem(
          dynamic_cast<CypherParser::OC_SetItemContext *>(child));
      auto props = p.as<properties_t>();
      for (auto &p : props) {
        // case 1: variable with properties -> we assign the property to the
        // variable in pmap
        if (boost::find_first(p.first, ".")) {
          add_to_properties(pmap, p.first, p.second);
        } else {
          // case 2: only variable
          // spdlog::info("=====> add property to variable {}", p.first);
          auto v = boost::any_cast<properties_t>(p.second);
          pattern_list.push_back(ast::node_pattern{p.first, "", v});
        }
      }
    }
  }
  // for each property map in pmap we create a node pattern
  for (auto &p : pmap) {
    pattern_list.push_back(ast::node_pattern{p.first, "", p.second});
  }
  return std::make_shared<ast::update_node>(ast::qnode_type::nt_set,
                                            pattern_list);
}

antlrcpp::Any
cypher_visitor::visitOC_SetItem(CypherParser::OC_SetItemContext *ctx) {
  // spdlog::info("SetItem");
  properties_t props;
  boost::any prop_val;
  std::string vname;
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_PropertyExpressionContext *>(child)) {
      vname = child->getText();
      // spdlog::info("SetItem: PropertyExpression: {}", vname);
    } else if (antlrcpp::is<CypherParser::OC_VariableContext *>(child)) {
      auto c = dynamic_cast<CypherParser::OC_VariableContext *>(child);
      vname = c->getText();
      // spdlog::info("SetItem: Variable: {}", vname);
    } else if (antlrcpp::is<CypherParser::OC_ExpressionContext *>(child)) {
      // spdlog::info("SetItem: Expression");
      auto expr = visitOC_Expression(
          dynamic_cast<CypherParser::OC_ExpressionContext *>(child));
      if (expr.is<properties_t>()) {
        auto props = expr.as<properties_t>();
        prop_val = props;
      } else if (expr.is<boost::any>()) {
        prop_val = expr.as<boost::any>();
      }
    } else if (antlrcpp::is<CypherParser::OC_NodeLabelsContext *>(child)) {
      // spdlog::info("SetItem: NodeLabels");
      auto labels = visitOC_NodeLabels(
          dynamic_cast<CypherParser::OC_NodeLabelsContext *>(child));
    }
  }
  props.insert({vname, prop_val});

  return props;
}

antlrcpp::Any
cypher_visitor::visitOC_Match(CypherParser::OC_MatchContext *ctx) {
  spdlog::info("Match");
  ast::plist pattern_list;
  ast::plist_set pset;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_PatternContext *>(child)) {
      auto pattern = visitOC_Pattern(
          dynamic_cast<CypherParser::OC_PatternContext *>(child));
      pset = pattern.as<ast::plist_set>();
    } else if (antlrcpp::is<CypherParser::OC_WhereContext *>(child)) {
    }
  }
  ast::qnode_list qnodes;
  for (auto &p : pset) {
    qnodes.push_back(std::make_shared<ast::match_node>(p));
  }
  return qnodes;
}

antlrcpp::Any
cypher_visitor::visitOC_InQueryCall(CypherParser::OC_InQueryCallContext *ctx) {
  // spdlog::info("InQueryCall");
  std::shared_ptr<ast::call_node> n;
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_ExplicitProcedureInvocationContext *>(
            child)) {
      auto cn = visitOC_ExplicitProcedureInvocation(
          dynamic_cast<CypherParser::OC_ExplicitProcedureInvocationContext *>(
              child));
      n = cn.as<std::shared_ptr<ast::call_node>>();
    } else if (antlrcpp::is<CypherParser::OC_YieldItemsContext *>(child)) {
      auto items = visitOC_YieldItems(
          dynamic_cast<CypherParser::OC_YieldItemsContext *>(child));
      n->yield_items(items.as<std::vector<std::string>>());
    }
  }
  return n;
}

antlrcpp::Any
cypher_visitor::visitOC_YieldItems(CypherParser::OC_YieldItemsContext *ctx) {
  // spdlog::info("YieldItems");
  std::vector<std::string> item_list;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_YieldItemContext *>(child)) {
      auto item = visitOC_YieldItem(
          dynamic_cast<CypherParser::OC_YieldItemContext *>(child));
      item_list.push_back(item.as<std::string>());
    }
  }
  return item_list;
}

antlrcpp::Any
cypher_visitor::visitOC_YieldItem(CypherParser::OC_YieldItemContext *ctx) {
  // spdlog::info("YieldItem {}", ctx->getText());
  return std::string(ctx->getText());
}

antlrcpp::Any cypher_visitor::visitOC_ExplicitProcedureInvocation(
    CypherParser::OC_ExplicitProcedureInvocationContext *ctx) {
  // spdlog::info("ExplicitProcedureInvocation");
  std::string proc_name;
  std::vector<ast::return_item> expr_list;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_ProcedureNameContext *>(child)) {
      proc_name = child->getText();
    } else if (antlrcpp::is<CypherParser::OC_ExpressionContext *>(child)) {
      // TODO: handle expressions
      auto ex = visitOC_Expression(
          dynamic_cast<CypherParser::OC_ExpressionContext *>(child));
      expr_list.push_back(ast::return_item{child->getText(), ""});
    }
  }
  return std::make_shared<ast::call_node>(proc_name, expr_list);
}

antlrcpp::Any
cypher_visitor::visitOC_ReturnBody(CypherParser::OC_ReturnBodyContext *ctx) {
  // spdlog::info("ReturnBody");
  auto ri = visitOC_ReturnItems(ctx->oC_ReturnItems());
  return std::make_shared<ast::return_node>(
      ri.as<std::vector<ast::return_item>>());
}

antlrcpp::Any
cypher_visitor::visitOC_ReturnItems(CypherParser::OC_ReturnItemsContext *ctx) {
  // spdlog::info("ReturnItems");
  std::vector<ast::return_item> ritem_list;
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_ReturnItemContext *>(child)) {
      auto ex = visitOC_ReturnItem(
          dynamic_cast<CypherParser::OC_ReturnItemContext *>(child));
      ritem_list.push_back(ex.as<ast::return_item>());
    }
  }
  return ritem_list;
}

antlrcpp::Any
cypher_visitor::visitOC_ReturnItem(CypherParser::OC_ReturnItemContext *ctx) {
  // spdlog::info("ReturnItem");
  std::string expr; // TODO: replace by expression!
  std::string var;
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_ExpressionContext *>(child)) {
      auto ex = visitOC_Expression(
          dynamic_cast<CypherParser::OC_ExpressionContext *>(child));
      expr = child->getText();
    } else if (antlrcpp::is<CypherParser::OC_VariableContext *>(child)) {
      var = child->getText();
    }
  }
  return ast::return_item{expr, var};
}

antlrcpp::Any
cypher_visitor::visitOC_Pattern(CypherParser::OC_PatternContext *ctx) {
  spdlog::info("Pattern");
  ast::plist_set pset;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_PatternPartContext *>(child)) {
      auto pl = visitOC_PatternPart(
          dynamic_cast<CypherParser::OC_PatternPartContext *>(child));
      pset.push_back(pl.as<ast::plist>());
    }
  }
  return pset;
}

antlrcpp::Any
cypher_visitor::visitOC_PatternPart(CypherParser::OC_PatternPartContext *ctx) {
  spdlog::info("PatternPart");
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_AnonymousPatternPartContext *>(child)) {
      return visitOC_AnonymousPatternPart(
          dynamic_cast<CypherParser::OC_AnonymousPatternPartContext *>(child));
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any cypher_visitor::visitOC_AnonymousPatternPart(
    CypherParser::OC_AnonymousPatternPartContext *ctx) {
  spdlog::info("AnonymousPatternPart");
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_PatternElementContext *>(child)) {
      return visitOC_PatternElement(
          dynamic_cast<CypherParser::OC_PatternElementContext *>(child));
    }
  }
  return antlrcpp::Any();
}

antlrcpp::Any cypher_visitor::visitOC_PatternElement(
    CypherParser::OC_PatternElementContext *ctx) {
  spdlog::info("PatternElement");
  ast::plist pattern_list;

  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_NodePatternContext *>(child)) {
      auto c = visitOC_NodePattern(
          dynamic_cast<CypherParser::OC_NodePatternContext *>(child));
      pattern_list.push_back(ast::pattern(c.as<ast::node_pattern>()));
    } else if (antlrcpp::is<CypherParser::OC_PatternElementChainContext *>(
                   child)) {
      auto c = visitOC_PatternElementChain(
          dynamic_cast<CypherParser::OC_PatternElementChainContext *>(child));
      std::vector<ast::pattern> other_list = c.as<std::vector<ast::pattern>>();
      pattern_list.reserve(pattern_list.size() + other_list.size());
      pattern_list.insert(pattern_list.end(), other_list.begin(),
                          other_list.end());
    }
  }
  return pattern_list;
}

antlrcpp::Any cypher_visitor::visitOC_PatternElementChain(
    CypherParser::OC_PatternElementChainContext *ctx) {
  // spdlog::info("PatternElementChain");
  ast::plist pattern_list;
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_NodePatternContext *>(child)) {
      auto c = visitOC_NodePattern(
          dynamic_cast<CypherParser::OC_NodePatternContext *>(child));
      pattern_list.push_back(ast::pattern(c.as<ast::node_pattern>()));
    } else if (antlrcpp::is<CypherParser::OC_LeftRelationshipPatternContext *>(
                   child)) {
      auto c = visitOC_LeftRelationshipPattern(
          dynamic_cast<CypherParser::OC_LeftRelationshipPatternContext *>(
              child));
      pattern_list.push_back(ast::pattern(c.as<ast::relationship_pattern>()));
    } else if (antlrcpp::is<CypherParser::OC_RightRelationshipPatternContext *>(
                   child)) {
      auto c = visitOC_RightRelationshipPattern(
          dynamic_cast<CypherParser::OC_RightRelationshipPatternContext *>(
              child));
      pattern_list.push_back(ast::pattern(c.as<ast::relationship_pattern>()));
    } else if (antlrcpp::is<CypherParser::OC_BiRelationshipPatternContext *>(
                   child)) {
      auto c = visitOC_BiRelationshipPattern(
          dynamic_cast<CypherParser::OC_BiRelationshipPatternContext *>(child));
      pattern_list.push_back(ast::pattern(c.as<ast::relationship_pattern>()));
    } else if (antlrcpp::is<CypherParser::OC_NoneRelationshipPatternContext *>(
                   child)) {
      auto c = visitOC_NoneRelationshipPattern(
          dynamic_cast<CypherParser::OC_NoneRelationshipPatternContext *>(
              child));
      pattern_list.push_back(ast::pattern(c.as<ast::relationship_pattern>()));
    }
  }
  return pattern_list;
}

antlrcpp::Any
cypher_visitor::visitOC_NodePattern(CypherParser::OC_NodePatternContext *ctx) {
  // spdlog::info("NodePattern");
  properties_t props;
  auto var = ctx->oC_Variable() ? ctx->oC_Variable()->getText() : "";
  auto labels = ctx->oC_NodeLabels();
  std::string label;
  if (labels != nullptr && labels->oC_NodeLabel().size() > 0)
    // ignore the leading ':' character
    label = labels->oC_NodeLabel(0)->getText().substr(1);
  if (ctx->oC_Properties() != nullptr) {
    auto p = visitOC_Properties(ctx->oC_Properties());
    props = p.as<properties_t>();
  }
  return ast::node_pattern{var, label, props};
}

ast::relationship_pattern cypher_visitor::create_relationship_pattern(
    ast::relationship_pattern::direction dir,
    CypherParser::OC_RelationshipDetailContext *ctx) {
  auto var = ctx->oC_Variable() ? ctx->oC_Variable()->getText() : "";

  auto labels = ctx->oC_RelationshipTypes();
  std::string label;
  if (labels != nullptr && labels->oC_RelTypeName().size() > 0)
    label = labels->oC_RelTypeName(0)->getText();

  auto range_literal = ctx->oC_RangeLiteral();
  auto range = std::make_pair(-1, -1);
  if (range_literal) {
    auto min_literal =
        std::stoi(range_literal->oC_IntegerLiteral(0)->getText());
    auto max_literal =
        std::stoi(range_literal->oC_IntegerLiteral(1)->getText());
    range.first = min_literal;
    range.second = max_literal;
  }

  properties_t props;
  if (ctx->oC_Properties() != nullptr) {
    auto p = visitOC_Properties(ctx->oC_Properties());
    props = p.as<properties_t>();
  }
  return ast::relationship_pattern(dir, var, label, range, props);
}

antlrcpp::Any cypher_visitor::visitOC_LeftRelationshipPattern(
    CypherParser::OC_LeftRelationshipPatternContext *ctx) {
  // spdlog::info("LeftRelationshipPattern");
  if (ctx->oC_RelationshipDetail() != nullptr) {
    return create_relationship_pattern(ast::relationship_pattern::right_to_left,
                                       ctx->oC_RelationshipDetail());
  }
  return ast::relationship_pattern(ast::relationship_pattern::right_to_left);
}

antlrcpp::Any cypher_visitor::visitOC_RightRelationshipPattern(
    CypherParser::OC_RightRelationshipPatternContext *ctx) {
  // spdlog::info("RightRelationshipPattern");
  if (ctx->oC_RelationshipDetail() != nullptr) {
    return create_relationship_pattern(ast::relationship_pattern::left_to_right,
                                       ctx->oC_RelationshipDetail());
  }
  return ast::relationship_pattern(ast::relationship_pattern::left_to_right);
}

antlrcpp::Any cypher_visitor::visitOC_BiRelationshipPattern(
    CypherParser::OC_BiRelationshipPatternContext *ctx) {
  // spdlog::info("BiRelationshipPattern");
  if (ctx->oC_RelationshipDetail() != nullptr) {
    return create_relationship_pattern(ast::relationship_pattern::both,
                                       ctx->oC_RelationshipDetail());
  }
  return ast::relationship_pattern(ast::relationship_pattern::both);
}
antlrcpp::Any cypher_visitor::visitOC_NoneRelationshipPattern(
    CypherParser::OC_NoneRelationshipPatternContext *ctx) {
  // spdlog::info("NoneRelationshipPattern");
  if (ctx->oC_RelationshipDetail() != nullptr) {
    return create_relationship_pattern(ast::relationship_pattern::none,
                                       ctx->oC_RelationshipDetail());
  }
  return ast::relationship_pattern(ast::relationship_pattern::none);
}

antlrcpp::Any
cypher_visitor::visitOC_Properties(CypherParser::OC_PropertiesContext *ctx) {
  // spdlog::info("Properties");
  if (ctx->oC_MapLiteral() != nullptr) {
    auto p = visitOC_MapLiteral(ctx->oC_MapLiteral());
    return p.as<properties_t>();
  } else if (ctx->oC_Parameter() != nullptr) {
    return antlrcpp::Any();
  }
  return antlrcpp::Any();
}

antlrcpp::Any
cypher_visitor::visitOC_MapLiteral(CypherParser::OC_MapLiteralContext *ctx) {
  // spdlog::info("MapLiteral");
  properties_t props;
  std::string prop_key;
  for (auto i = 0u; i < ctx->children.size(); i++) {
    auto child = ctx->children[i];
    if (antlrcpp::is<CypherParser::OC_PropertyKeyNameContext *>(child)) {
      prop_key = child->getText();
    } else if (antlrcpp::is<CypherParser::OC_ExpressionContext *>(child)) {
      auto ex = visitOC_Expression(
          dynamic_cast<CypherParser::OC_ExpressionContext *>(child));
      auto prop_val = ex.as<boost::any>();
      // spdlog::info("MapLiteral: add {} to properties", prop_key);
      props.insert({prop_key, prop_val});
    }
  }
  return props;
}

antlrcpp::Any
cypher_visitor::visitOC_Expression(CypherParser::OC_ExpressionContext *ctx) {
  // spdlog::info("TODO: Expression: {}", ctx->getText());
  auto s = ctx->getText();
  boost::any ret;
  if (is_quoted_string(s))
    ret = s.substr(1, s.length() - 2);
  else if (is_int(s))
    ret = (int)std::stoi(s);
  else if (is_float(s))
    ret = (double)std::stod(s);
  else if (ctx->oC_OrExpression() != nullptr) {
    auto res = visitOC_OrExpression(ctx->oC_OrExpression());
    // spdlog::info("Expression: result isNotNull {}", res.isNotNull());
    if (res.is<properties_t>()) {
      auto props = res.as<properties_t>();
      // spdlog::info("Expression: result props {}", props.size());
    }
    return res;
  } else {
    // TODO: handle the case of oC_PropertyOrLabelsExpression!!
    // spdlog::info("other expression: {}", s);
    auto res = visitChildren(ctx);
    // spdlog::info("other expression: result isNotNull {}",
    // res.isNotNull());
    return res;
  }

  return antlrcpp::Any(ret);
}

antlrcpp::Any cypher_visitor::visitOC_OrExpression(
    CypherParser::OC_OrExpressionContext *ctx) {
  // spdlog::info("OrExpression");
  return visitOC_XorExpression(ctx->oC_XorExpression(0));
}

antlrcpp::Any cypher_visitor::visitOC_XorExpression(
    CypherParser::OC_XorExpressionContext *ctx) {
  // spdlog::info("XorExpression");
  return visitOC_AndExpression(ctx->oC_AndExpression(0));
}

antlrcpp::Any cypher_visitor::visitOC_AndExpression(
    CypherParser::OC_AndExpressionContext *ctx) {
  // spdlog::info("AndExpression");
  return visitOC_NotExpression(ctx->oC_NotExpression(0));
}

antlrcpp::Any cypher_visitor::visitOC_NotExpression(
    CypherParser::OC_NotExpressionContext *ctx) {
  // spdlog::info("NotExpression");
  return visitOC_ComparisonExpression(ctx->oC_ComparisonExpression());
}

antlrcpp::Any cypher_visitor::visitOC_ComparisonExpression(
    CypherParser::OC_ComparisonExpressionContext *ctx) {
  // spdlog::info("ComparisonExpression");
  if (ctx->oC_AddOrSubtractExpression() != nullptr) {
    auto res =
        visitOC_AddOrSubtractExpression(ctx->oC_AddOrSubtractExpression());
    // spdlog::info("ComparisonExpression: result isNotNull {}",
    // res.isNotNull());
    return res;
  } else
    return visitChildren(ctx);
}

antlrcpp::Any cypher_visitor::visitOC_AddOrSubtractExpression(
    CypherParser::OC_AddOrSubtractExpressionContext *ctx) {
  // spdlog::info("AddOrSubtractExpression");
  return visitOC_MultiplyDivideModuloExpression(
      ctx->oC_MultiplyDivideModuloExpression(0));
}

antlrcpp::Any cypher_visitor::visitOC_MultiplyDivideModuloExpression(
    CypherParser::OC_MultiplyDivideModuloExpressionContext *ctx) {
  // spdlog::info("MultiplyDivideModuloExpression");
  return visitOC_PowerOfExpression(ctx->oC_PowerOfExpression(0));
}

antlrcpp::Any cypher_visitor::visitOC_PowerOfExpression(
    CypherParser::OC_PowerOfExpressionContext *ctx) {
  // spdlog::info("PowerOfExpression");
  auto res = visitOC_UnaryAddOrSubtractExpression(
      ctx->oC_UnaryAddOrSubtractExpression(0));
  // spdlog::info("PowerOfExpression: result isNotNull {}",
  // res.isNotNull());
  return res;
}

antlrcpp::Any cypher_visitor::visitOC_UnaryAddOrSubtractExpression(
    CypherParser::OC_UnaryAddOrSubtractExpressionContext *ctx) {
  // spdlog::info("UnaryAddOrSubtractExpression");
  auto res = visitOC_StringListNullOperatorExpression(
      ctx->oC_StringListNullOperatorExpression());
  // spdlog::info("UnaryAddOrSubtractExpression: result isNotNull {}",
  //             res.isNotNull());
  return res;
}

antlrcpp::Any cypher_visitor::visitOC_StringListNullOperatorExpression(
    CypherParser::OC_StringListNullOperatorExpressionContext *ctx) {
  // spdlog::info("StringListNullOperatorExpression");
  if (ctx->oC_PropertyOrLabelsExpression() != nullptr) {
    // spdlog::info(
    //    "StringListNullOperatorExpression: PropertyOrLabelsExpression");
    auto res = visitOC_PropertyOrLabelsExpression(
        ctx->oC_PropertyOrLabelsExpression());
    // spdlog::info("StringListNullOperatorExpression: result isNotNull {}",
    //             res.isNotNull());
    if (res.is<properties_t>()) {
      auto props = res.as<properties_t>();
      // spdlog::info("StringListNullOperatorExpression: props = {}",
      // props.size());
    }
    return res;
  } else
    return visitChildren(ctx);
}

antlrcpp::Any cypher_visitor::visitOC_ListOperatorExpression(
    CypherParser::OC_ListOperatorExpressionContext *ctx) {
  // spdlog::info("ListOperatorExpression");
  return visitChildren(ctx);
}

antlrcpp::Any cypher_visitor::visitOC_StringOperatorExpression(
    CypherParser::OC_StringOperatorExpressionContext *ctx) {
  // spdlog::info("StringOperatorExpression");
  return visitChildren(ctx);
}

antlrcpp::Any cypher_visitor::visitOC_NullOperatorExpression(
    CypherParser::OC_NullOperatorExpressionContext *ctx) {
  // spdlog::info("NullOperatorExpression");
  return visitChildren(ctx);
}

antlrcpp::Any cypher_visitor::visitOC_PropertyOrLabelsExpression(
    CypherParser::OC_PropertyOrLabelsExpressionContext *ctx) {
  // spdlog::info("PropertyOrLabelsExpression");
  if (ctx->oC_Atom() != nullptr) {
    // spdlog::info("PropertyOrLabelsExpression: Atom");
    auto r = visitOC_Atom(ctx->oC_Atom());
    // spdlog::info("PropertyOrLabelsExpression: result isNotNull {}",
    //              r.isNotNull());
    if (r.is<properties_t>()) {
      // TODO: not always properties_t
      auto props = r.as<properties_t>();
      // spdlog::info("PropertyOrLabelsExpression: props = {}",
      // props.size());
    }
    return r;
  } else
    return visitChildren(ctx);
}

antlrcpp::Any cypher_visitor::visitOC_Atom(CypherParser::OC_AtomContext *ctx) {
  // spdlog::info("Atom");
  if (ctx->oC_Literal() != nullptr) {
    // spdlog::info("Atom: Literal");
    auto r = visitOC_Literal(ctx->oC_Literal());
    assert(r.isNotNull());
    // spdlog::info("Atom: result isNotNull");
    auto props = r.as<properties_t>();
    // spdlog::info("Atom: props = {}", props.size());
    return props;
  } else if (ctx->oC_Variable() != nullptr) {
    // spdlog::info("Atom: variable {}", ctx->oC_Variable()->getText());
    return ctx->oC_Variable()->getText();
    // return visitOC_Variable(ctx->oC_Variable());
  } else {
    // spdlog::info("Atom: {}", ctx->getText());
    return visitChildren(ctx);
  }
}

antlrcpp::Any
cypher_visitor::visitOC_Literal(CypherParser::OC_LiteralContext *ctx) {
  // spdlog::info("Literal");
  if (ctx->oC_MapLiteral() != nullptr) {
    auto a = visitOC_MapLiteral(ctx->oC_MapLiteral());
    assert(a.isNotNull());
    // spdlog::info("Literal: result isNotNull");
    return a.as<properties_t>();
  } else
    return visitChildren(ctx);
}
