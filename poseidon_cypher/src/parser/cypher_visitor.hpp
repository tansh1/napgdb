#ifndef cypher_visitor_hpp_
#define cypher_visitor_hpp_

#include "CypherBaseVisitor.h"
#include "CypherLexer.h"
#include "CypherParser.h"

#include "ast.hpp"

class cypher_visitor : public parser::CypherBaseVisitor {
public:
  cypher_visitor();

  antlrcpp::Any
  visitOC_Cypher(parser::CypherParser::OC_CypherContext *ctx) override;

  antlrcpp::Any
  visitOC_Statement(parser::CypherParser::OC_StatementContext *ctx) override;

  antlrcpp::Any
  visitOC_Query(parser::CypherParser::OC_QueryContext *ctx) override;

  antlrcpp::Any visitOC_RegularQuery(
      parser::CypherParser::OC_RegularQueryContext *ctx) override;

  antlrcpp::Any visitOC_SingleQuery(
      parser::CypherParser::OC_SingleQueryContext *ctx) override;

  antlrcpp::Any visitOC_SinglePartQuery(
      parser::CypherParser::OC_SinglePartQueryContext *ctx) override;

  antlrcpp::Any visitOC_ReadingClause(
      parser::CypherParser::OC_ReadingClauseContext *ctx) override;

  antlrcpp::Any visitOC_UpdatingClause(
      parser::CypherParser::OC_UpdatingClauseContext *ctx) override;

  antlrcpp::Any
  visitOC_Create(parser::CypherParser::OC_CreateContext *ctx) override;

  antlrcpp::Any visitOC_Set(parser::CypherParser::OC_SetContext *ctx) override;

  antlrcpp::Any
  visitOC_SetItem(parser::CypherParser::OC_SetItemContext *ctx) override;

  antlrcpp::Any
  visitOC_Match(parser::CypherParser::OC_MatchContext *ctx) override;

  antlrcpp::Any visitOC_InQueryCall(
      parser::CypherParser::OC_InQueryCallContext *ctx) override;

  antlrcpp::Any visitOC_ExplicitProcedureInvocation(
      parser::CypherParser::OC_ExplicitProcedureInvocationContext *ctx)
      override;

  antlrcpp::Any
  visitOC_YieldItems(parser::CypherParser::OC_YieldItemsContext *ctx) override;

 antlrcpp::Any
  visitOC_YieldItem(parser::CypherParser::OC_YieldItemContext *ctx) override;

  antlrcpp::Any
  visitOC_ReturnBody(parser::CypherParser::OC_ReturnBodyContext *ctx) override;

  antlrcpp::Any visitOC_ReturnItems(
      parser::CypherParser::OC_ReturnItemsContext *ctx) override;

  antlrcpp::Any
  visitOC_ReturnItem(parser::CypherParser::OC_ReturnItemContext *ctx) override;

  antlrcpp::Any
  visitOC_Pattern(parser::CypherParser::OC_PatternContext *ctx) override;

  antlrcpp::Any visitOC_PatternPart(
      parser::CypherParser::OC_PatternPartContext *ctx) override;

  antlrcpp::Any visitOC_AnonymousPatternPart(
      parser::CypherParser::OC_AnonymousPatternPartContext *ctx) override;

  antlrcpp::Any visitOC_PatternElement(
      parser::CypherParser::OC_PatternElementContext *ctx) override;

  antlrcpp::Any visitOC_PatternElementChain(
      parser::CypherParser::OC_PatternElementChainContext *ctx) override;

  antlrcpp::Any visitOC_NodePattern(
      parser::CypherParser::OC_NodePatternContext *ctx) override;

  antlrcpp::Any visitOC_BiRelationshipPattern(
      parser::CypherParser::OC_BiRelationshipPatternContext *ctx) override;

  antlrcpp::Any visitOC_LeftRelationshipPattern(
      parser::CypherParser::OC_LeftRelationshipPatternContext *ctx) override;

  antlrcpp::Any visitOC_RightRelationshipPattern(
      parser::CypherParser::OC_RightRelationshipPatternContext *ctx) override;

  antlrcpp::Any visitOC_NoneRelationshipPattern(
      parser::CypherParser::OC_NoneRelationshipPatternContext *ctx) override;

  antlrcpp::Any
  visitOC_Properties(parser::CypherParser::OC_PropertiesContext *ctx) override;

  antlrcpp::Any
  visitOC_MapLiteral(parser::CypherParser::OC_MapLiteralContext *ctx) override;

  antlrcpp::Any
  visitOC_Expression(parser::CypherParser::OC_ExpressionContext *ctx) override;

  antlrcpp::Any visitOC_OrExpression(
      parser::CypherParser::OC_OrExpressionContext *ctx) override;

  antlrcpp::Any visitOC_XorExpression(
      parser::CypherParser::OC_XorExpressionContext *ctx) override;

  antlrcpp::Any visitOC_AndExpression(
      parser::CypherParser::OC_AndExpressionContext *ctx) override;

  antlrcpp::Any visitOC_NotExpression(
      parser::CypherParser::OC_NotExpressionContext *ctx) override;

  antlrcpp::Any visitOC_ComparisonExpression(
      parser::CypherParser::OC_ComparisonExpressionContext *ctx) override;

  antlrcpp::Any visitOC_AddOrSubtractExpression(
      parser::CypherParser::OC_AddOrSubtractExpressionContext *ctx) override;

  antlrcpp::Any visitOC_MultiplyDivideModuloExpression(
      parser::CypherParser::OC_MultiplyDivideModuloExpressionContext *ctx)
      override;

  antlrcpp::Any visitOC_PowerOfExpression(
      parser::CypherParser::OC_PowerOfExpressionContext *ctx) override;

  antlrcpp::Any visitOC_UnaryAddOrSubtractExpression(
      parser::CypherParser::OC_UnaryAddOrSubtractExpressionContext *ctx)
      override;

  antlrcpp::Any visitOC_StringListNullOperatorExpression(
      parser::CypherParser::OC_StringListNullOperatorExpressionContext *ctx)
      override;

  antlrcpp::Any visitOC_ListOperatorExpression(
      parser::CypherParser::OC_ListOperatorExpressionContext *ctx) override;

  antlrcpp::Any visitOC_StringOperatorExpression(
      parser::CypherParser::OC_StringOperatorExpressionContext *ctx) override;

  antlrcpp::Any visitOC_NullOperatorExpression(
      parser::CypherParser::OC_NullOperatorExpressionContext *ctx) override;

  antlrcpp::Any visitOC_PropertyOrLabelsExpression(
      parser::CypherParser::OC_PropertyOrLabelsExpressionContext *ctx) override;

  antlrcpp::Any
  visitOC_Atom(parser::CypherParser::OC_AtomContext *ctx) override;

  antlrcpp::Any
  visitOC_Literal(parser::CypherParser::OC_LiteralContext *ctx) override;

  /* --------- */
  ast::relationship_pattern create_relationship_pattern(
      ast::relationship_pattern::direction dir,
      parser::CypherParser::OC_RelationshipDetailContext *ctx);

private:
  void add_to_properties(std::map<std::string, properties_t> &pmap,
                         const std::string &pkey, boost::any &val);
};

#endif