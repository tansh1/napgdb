#include <boost/dynamic_bitset.hpp>
#include <boost/hana.hpp>
#include <iostream>
#include <memory>
#include <sstream>

#include "CypherLexer.h"
#include "CypherParser.h"
#include "ast.hpp"
#include "cypher_visitor.hpp"
#include "properties.hpp"
#include "query.hpp"
#include "query_compiler.hpp"

using namespace antlr4;
/* ------------------------------------------------------------------ */

class ParserErrorListener : public BaseErrorListener {
public:
  ParserErrorListener() = default;

  virtual void syntaxError(Recognizer *recognizer, Token *offendingSymbol,
                           size_t line, size_t charPositionInLine,
                           const std::string &msg,
                           std::exception_ptr e) override;
};

void ParserErrorListener::syntaxError(Recognizer *recognizer,
                                      Token *offendingSymbol, size_t line,
                                      size_t charPositionInLine,
                                      const std::string &msg,
                                      std::exception_ptr e) {
  std::ostringstream s;
  s << "Line(" << line << ":" << charPositionInLine << ") Error(" << msg << ")";
  throw std::invalid_argument(s.str());
}

ast::query_ptr query_compiler::parse(const std::string &query) {
  ANTLRInputStream input(query);
  parser::CypherLexer lexer(&input);
  CommonTokenStream tokens(&lexer);
  parser::CypherParser parser(&tokens);
  ParserErrorListener parserErrorListener;

  parser.removeParseListeners();
  parser.removeErrorListeners();
  parser.addErrorListener(&parserErrorListener);
  parser::CypherParser::OC_CypherContext *tree = nullptr;
  spdlog::info("parse query '{}'", query);
  try {
    tree = parser.oC_Cypher();
  } catch (std::invalid_argument &e) {
    std::cout << e.what() << std::endl;
    return nullptr;
  }
  cypher_visitor visitor;
  auto res = visitor.visitOC_Cypher(tree);
  auto ast_root = res.as<std::shared_ptr<ast::query_node>>();
  ast_root->print();
  return ast_root;
}

std::pair<std::string, std::function<bool(const p_item &)>>
query_compiler::property_filter(const properties_t props) {
  std::function<bool(const p_item &)> pred_func;
  auto p = props.begin();
  auto pv = p->second;
  if (pv.type() == typeid(p_item::int_t)) {
    auto v = boost::any_cast<p_item::int_t>(pv);
    pred_func = [v](auto item) { return item.equal(v); };
  } else if (pv.type() == typeid(p_item::double_t)) {
    auto v = boost::any_cast<p_item::double_t>(pv);
    pred_func = [v](auto item) { return item.equal(v); };
  } else if (pv.type() == typeid(p_item::string_t)) {
    auto vs = boost::any_cast<p_item::string_t>(pv);
    auto v = graph_->get_code(vs);
    pred_func = [v](auto item) { return item.equal(v); };
  }
  return std::make_pair(p->first, pred_func);
}

void query_compiler::match_to_plan(query &q,
                                   std::shared_ptr<ast::match_node> ast_node) {
  std::size_t pindex = 0;
  ast::relationship_pattern::direction last_direction =
      ast::relationship_pattern::undefined;
  for (auto &p : ast_node->pattern_list_) {
    if (p.which() == 0) {
      auto &n = boost::get<ast::node_pattern>(p);
      // if this is the first node pattern we can add all_nodes or nodes_where
      if (pindex == 0) {
        if (n.props.empty())
          q.all_nodes(n.label);
        else {
          // TODO: currently, we consider only the first property
          auto pres = property_filter(n.props);
          q.nodes_where(n.label, pres.first, pres.second);
        }
      } else {
        // otherwise, we add to_node or from_node depending on the direction of
        // the previous relationship pattern
        switch (last_direction) {
        case ast::relationship_pattern::left_to_right:
          q.to_node(n.label);
          break;
        case ast::relationship_pattern::right_to_left:
          q.from_node(n.label);
          break;
        default:
          // TODO
          break;
        }
        // add has_property
        if (!n.props.empty()) {
          auto pres = property_filter(n.props);
          q.property(pres.first, pres.second);
        }
      }
    } else {
      auto &r = boost::get<ast::relationship_pattern>(p);
      switch (r.dir) {
        // handle range specification
      case ast::relationship_pattern::left_to_right:
        if (r.range.first != -1 || r.range.second != -1)
          q.from_relationships(r.range, r.label);
        else
          q.from_relationships(r.label);
        break;
      case ast::relationship_pattern::right_to_left:
        if (r.range.first != -1 || r.range.second != -1)
          q.to_relationships(r.range, r.label);
        else
          q.to_relationships(r.label);
        break;
      default:
        // TODO
        break;
      }
      last_direction = r.dir;
    }
    pindex++;
  }
}

void query_compiler::call_to_plan(query &q,
                                  std::shared_ptr<ast::call_node> ast_node,
                                  var_list &variables) {
  std::vector<std::size_t> params;
  if (!ast_node->args_.empty()) {
    for (auto &arg : ast_node->args_) {
      auto dpos = arg.expr.find(".");
      if (dpos == std::string::npos) {
        // node/relationship variable - we simply return the
        // node/relationship object here
        auto var_name = arg.expr;
        std::size_t vidx = find_variable(variables, var_name);
        params.push_back(vidx);
      } else {
        // TODO: property variable - actually, we should insert a projection!!
        auto var_name = arg.expr.substr(0, dpos);
        auto key = arg.expr.substr(dpos + 1);
        std::size_t vidx = find_variable(variables, var_name);
        params.push_back(vidx);
      }
    }
  }
  // handle YIELD
  std::size_t next = variables.size();
  for (auto &yi : ast_node->yitems_) {
    variables.insert({yi, next++});
  }
  // q.call_lua(ast_node->proc_name_, params);
}

void query_compiler::return_to_plan(query &q,
                                    std::shared_ptr<ast::return_node> ast_node,
                                    var_list &variables, result_set *rs) {
  projection::expr_list pexprs;

  if (!ast_node->ritems_.empty()) {
    for (auto &ri : ast_node->ritems_) {
      auto dpos = ri.expr.find(".");
      if (dpos == std::string::npos) {
        // node/relationship variable - we simply return the
        // node/relationship object here
        auto var_name = ri.expr;
        std::size_t vidx = find_variable(variables, var_name);
        pexprs.push_back(projection::expr{vidx, [](auto res) -> query_result {
                                            return builtin::forward(res);
                                          }});
        // vidx, nullptr});
      } else {
        // TODO: property variable - don't convert everything to a string
        auto var_name = ri.expr.substr(0, dpos);
        auto key = ri.expr.substr(dpos + 1);
        std::size_t vidx = find_variable(variables, var_name);
        pexprs.push_back(
            projection::expr{vidx, [key](auto res) -> query_result {
                               return builtin::string_property(res, key);
                             }});
      }
    }
    q.project(pexprs);
  }
  if (rs != nullptr)
    q.collect(*rs);
  else
    q.print();
}

void query_compiler::update_to_plan(query &q,
                                    std::shared_ptr<ast::update_node> ast_node,
                                    var_list &variables) {
  if (ast_node->type() == ast::qnode_type::nt_create) {
    spdlog::info("create node/relationship");
    // we collect all patterns containing variables which haven't been
    // introduced before, i.e. are in the variables list
    boost::dynamic_bitset<> vbit(ast_node->pattern_list_.size());
    var_list variables2;
    extract_variables<ast::update_node>(
        std::dynamic_pointer_cast<ast::update_node>(ast_node), variables2);
    for (auto v : variables2) {
      auto vname = v.first;
      if (variables.find(vname) != variables.end()) {
        spdlog::info("variable: {} @{}", vname, v.second);
        vbit[v.second] = 1;
      }
    }

    for (size_t i = 0; i < ast_node->pattern_list_.size(); i++) {
      if (!vbit[i]) {
        auto &p = ast_node->pattern_list_[i];
        if (p.which() == 0) {
          // node_pattern
          auto &np = boost::get<ast::node_pattern &>(p);
          q.create(np.label, np.props);
        } else if (p.which() == 1) {
          // relationship_pattern
          auto &rp = boost::get<ast::relationship_pattern &>(p);
          // TODO: get node_id
          // q.create_rship(rp.label, rp.props);
        }
      }
    }
    /*
        auto my_visitor = boost::hana::overload(
            [&](ast::node_pattern &n) { q.create(n.label, n.props); },
            [&](ast::relationship_pattern &r) {
              // TODO: create relationship q.create(r.);
            });

        for (auto &p : ast_node->pattern_list_) {
          boost::apply_visitor(my_visitor, p);
        }
        */
  } else if (ast_node->type() == ast::qnode_type::nt_set) {
    auto my_visitor = boost::hana::overload(
        [&](ast::node_pattern &n) {
          auto var = find_variable(variables, n.variable);
          q.update(var, n.props);
        },
        [&](ast::relationship_pattern &r) {
          // TODO: update relationship q.update(r.);
        });

    for (auto &p : ast_node->pattern_list_) {
      boost::apply_visitor(my_visitor, p);
    }
  }
}

void query_compiler::join_plans(query_set &qset) {
  qset.at(0).crossjoin(qset.at(1));
}

query_set query_compiler::ast_to_plan(ast::query_ptr qp, result_set *rs) {
  // first, we process reading nodes which "produce" data
  query_set qset;
  var_list variables;

  for (auto n : qp->reading_nodes_) {
    if (n->type() == ast::qnode_type::nt_match) {
      query q(graph_);
      extract_variables<ast::match_node>(
          std::dynamic_pointer_cast<ast::match_node>(n), variables);
      match_to_plan(q, std::dynamic_pointer_cast<ast::match_node>(n));
      qset.add(q);
    } else if (n->type() == ast::qnode_type::nt_call) {
      // TODO: when should we create a new query??
      if (qset.empty()) {
        query q(graph_);
        call_to_plan(q, std::dynamic_pointer_cast<ast::call_node>(n),
                     variables);
        qset.add(q);
      } else {
        call_to_plan(qset.front(), std::dynamic_pointer_cast<ast::call_node>(n),
                     variables);
      }
    }
  }
  if (qset.size() > 1) {
    // if we have an appropriate join condition, we create a join, otherwise a
    // cross join
    spdlog::info("JOIN!!!!!!!");
    join_plans(qset);
  }
  for (auto n : qp->update_nodes_) {
    if (qset.empty()) {
      query q(graph_);
      update_to_plan(q, std::dynamic_pointer_cast<ast::update_node>(n),
                     variables);
      qset.add(q);
    } else {
      // handle updates as part of reading queries
      update_to_plan(qset.front(),
                     std::dynamic_pointer_cast<ast::update_node>(n), variables);
    }
  }
  if (qp->return_node_)
    return_to_plan(qset.front(), qp->return_node_, variables, rs);
  spdlog::info("query_set: {}", qset.size());
  return qset;
}

std::size_t query_compiler::find_variable(var_list &vars,
                                          const std::string &vname) {
  auto it = vars.find(vname);
  if (it == vars.end())
    throw std::range_error("unknown variable");
  return it->second;
}

/*
void query_compiler::extract_variables(
    std::shared_ptr<ast::match_node> ast_node, var_list &vars) {
  std::size_t next = vars.size();
  auto my_visitor = boost::hana::overload(
      [&](ast::node_pattern &n) {
        if (!n.variable.empty())
          vars.insert({n.variable, next});
        next++;
      },
      [&](ast::relationship_pattern &r) {
        if (!r.variable.empty())
          vars.insert({r.variable, next});
        next++;
      });

  for (auto &p : ast_node->pattern_list_) {
    boost::apply_visitor(my_visitor, p);
  }
}
*/
