#ifndef query_compiler_hpp_
#define query_compiler_hpp_

#include "ast.hpp"
#include "dict.hpp"
#include "qop.hpp"
#include "query.hpp"
#include <boost/hana.hpp>

/**
 * query_compiler implements a compiler for the Cypher query language. It
 * supports parsing query strings as well as the construction of query execution
 * plans.
 */
class query_compiler {
  using var_list = std::map<std::string, std::size_t>;

public:
  /**
   * Constructor for a new query compiler.
   */
  query_compiler(graph_db_ptr gdb) : graph_(gdb) {}

  /**
   * Parses the given query string and returns an abstract syntax tree.
   */
  ast::query_ptr parse(const std::string &query);

  /**
   * Transforms the given AST into an executable query plan.
   */
  query_set ast_to_plan(ast::query_ptr qp, result_set *rs = nullptr);

private:
  /**
   * Handle a match_node in the AST by adding the corresponding operators to the
   * plan.
   */
  void match_to_plan(query &q, std::shared_ptr<ast::match_node> ast_node);

  /**
   * Handle a call_node in the AST by adding the corresponding operators to the
   * plan.
   */
  void call_to_plan(query &q, std::shared_ptr<ast::call_node> ast_node,
                    var_list &variables);

  /**
   * Handle a return_node in the AST by adding the corresponding operators to
   * the plan. If a result_set is given, a collect operator is added, too.
   */
  void return_to_plan(query &q, std::shared_ptr<ast::return_node> ast_node,
                      var_list &variables, result_set *rs);

  /**
   * Handle a update_node in the AST by adding the corresponding operators to
   * the plan.
   */
  void update_to_plan(query &q, std::shared_ptr<ast::update_node> ast_node,
                      var_list &variables);

  /**
   * TODO
   */
  void join_plans(query_set &qset);

  /**
   * Collect the list of all variables in the match_node and add them together
   * with their position to the vars map.
   */
  template <typename T>
  void extract_variables(std::shared_ptr<T> ast_node, var_list &vars) {
    std::size_t next = vars.size();
    auto my_visitor = boost::hana::overload(
        [&](ast::node_pattern &n) {
          if (!n.variable.empty())
            vars.insert({n.variable, next});
          next++;
        },
        [&](ast::relationship_pattern &r) {
          if (!r.variable.empty())
            vars.insert({r.variable, next});
          next++;
        });

    for (auto &p : ast_node->pattern_list_) {
      boost::apply_visitor(my_visitor, p);
    }
  }

  /**
   * Find the variable with the given name vname in the list of variables and
   * return its position in the result tuple.
   */
  std::size_t find_variable(var_list &vars, const std::string &vname);

  /**
   * Handle a property in a match_node of the AST by creating an appropriate
   * filter function.
   */
  std::pair<std::string, std::function<bool(const p_item &)>>
  property_filter(const properties_t props);

  graph_db_ptr graph_; // pointer to the graph database
};

#endif
