
#include <chrono>
#include <iostream>
#include <map>
#include <thread>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/any.hpp>
#include <boost/program_options.hpp>

#include "defs.hpp"
#include "graph_db.hpp"
#include "ldbc.hpp"
#include "linenoise.hpp"
#include "qop.hpp"
#include "query_compiler.hpp"

#include "threadsafe_queue.hpp"

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/spdlog.h"

#ifdef USE_PMDK

#define POOL_SIZE ((unsigned long long)(1024 * 1024 * 10000ull)) // 4000 MiB

// #define PMEM_PATH "/mnt/pmem0/poseidon/"
#define PMEM_PATH "/"

struct root {
  graph_db_ptr graph;
};

#endif

using namespace boost::program_options;

void exec_query(graph_db_ptr &gdb, const std::string &qstr);

/* ---------------------- session handling ---------------------- */

/**
 * msg is a structure for exchanging data between the main threads
 * an the session threads.
 */
struct msg {
  enum msg_type { BOT, Commit, Abort, ExecPlan };

  /**
   * Constructor and desctructor.
   */
  msg(msg_type m, const std::string &qstr = "") : mtype(m), query(qstr) {}
  ~msg() = default;

  msg_type mtype;    // the type of message
  std::string query; // the query to be executed if mtype == ExecPlan
};

/**
 * session represents the thread in those context a transaction is
 * executed. The underlying thread loops forever, waits for messages
 * via its channel, and executes the requested commands (BOT, commit,
 * execute plan etc.) within its context.
 */
class session {
public:
  /**
   * Constructor.
   */
  session(graph_db_ptr &graph_db) : gdb(graph_db) {}

  /**
   * Return the channel (message queue) to communicate with
   * the thread.
   */
  threadsafe_queue<msg> &channel() { return queue; }

  void spawn() {
    thr = std::thread([=] { run(); });
  }

  /**
   * Send an Abort to the session transaction and wait for
   * end of the thread.
   */
  void exit() {
    queue.push(msg(msg::Abort));
    thr.join();
  }

private:
  /**
   * The actual worker routine of the thread.
   */
  void run() {
    spdlog::debug("spawning a new thread...");
    for (;;) {
      auto m = queue.wait_and_pop();
      switch (m->mtype) {
      case msg::BOT:
        tx = gdb->begin_transaction();
        spdlog::debug("begin_transaction");
        break;
      case msg::Commit:
        gdb->commit_transaction();
        spdlog::debug("commit_transaction");
        return;
      case msg::Abort:
        gdb->abort_transaction();
        spdlog::debug("abort_transaction");
        return;
      case msg::ExecPlan:
        spdlog::debug("execute query");
        exec_query(gdb, m->query);
        break;
      }
    }
  }

  graph_db_ptr &gdb;  // the graph_db instance we are working on
  transaction_ptr tx; // the transaction associated with this session
  threadsafe_queue<msg>
      queue;       // the channel for communicating with the main thread
  std::thread thr; // the actual thread
};

using session_ptr = std::shared_ptr<session>;

std::map<std::size_t, session_ptr> session_list;
std::size_t current_session = 0, last_session = 0;

/**
 * Call exit on all sessions in the list.
 */
void end_sessions() {
  for (auto p : session_list) {
    p.second->exit();
  }
}

/* -------------------------------------------------------------- */

/**
 * Import data from the given list of CSV files. The list contains
 * not only the files names but also nodes/relationships as well as
 * the labels.
 */
bool import_csv_files(graph_db_ptr &gdb, const std::vector<std::string> &files,
                      char delimiter) {
  graph_db::mapping_t id_mapping;

  for (auto s : files) {
    if (s.find("nodes:") != std::string::npos) {
      std::vector<std::string> result;
      boost::split(result, s, boost::is_any_of(":"));

      if (result.size() != 3) {
        std::cerr << "ERROR: unknown import option for nodes." << std::endl;
        return false;
      }

      auto start = std::chrono::steady_clock::now();
      auto num = gdb->import_nodes_from_csv(result[1], result[2], delimiter,
                                            id_mapping);
      auto end = std::chrono::steady_clock::now();

      std::cout << num << " nodes of type '" << result[1] << "' imported in "
                << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                         start)
                       .count()
                << " msecs." << std::endl;
    } else if (s.find("relationships:") != std::string::npos) {
      std::vector<std::string> result;
      boost::split(result, s, boost::is_any_of(":"));

      if (result.size() != 2) {
        std::cerr << "ERROR: unknown import option for relationships."
                  << std::endl;
        return false;
      }

      auto start = std::chrono::steady_clock::now();
      auto num =
          gdb->import_relationships_from_csv(result[1], delimiter, id_mapping);
      auto end = std::chrono::steady_clock::now();

      std::cout << num << " relationships imported in "
                << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                         start)
                       .count()
                << " msecs." << std::endl;
    } else {
      std::cerr << "ERROR: unknown import (nodes or relationships expected)."
                << std::endl;
      return false;
    }
  }
  return true;
}

/**
 * Remove leading and trailing whitespaces from the given string.
 */
static void trim(std::string &s) {
  s.erase(s.begin(), std::find_if_not(s.begin(), s.end(),
                                      [](char c) { return std::isspace(c); }));
  s.erase(std::find_if_not(s.rbegin(), s.rend(),
                           [](char c) { return std::isspace(c); })
              .base(),
          s.end());
}

/**
 *
 */
void exec_query(graph_db_ptr &gdb, const std::string &qstr) {
  query_compiler qcomp(gdb);

  auto ast = qcomp.parse(qstr);
  if (ast == nullptr) {
    std::cerr << "ERROR: invalid query.\n";
    return;
  }

  auto qplan = qcomp.ast_to_plan(ast);

  std::ostringstream os;
  os << "Execution plan: '";
  qplan.dump(os);
  os << "'";
  spdlog::debug(os.str());

  auto start_qp = std::chrono::steady_clock::now();
  try {
    qplan.start();
  } catch (std::exception &exc) {
    spdlog::info("exception raised: {}", exc.what());
  }
  auto end_qp = std::chrono::steady_clock::now();

  std::cout << "Query executed in "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end_qp -
                                                                     start_qp)
                   .count()
            << " ms" << std::endl;
}

/**
 * Print help information to the console.
 */
void print_help() {
  std::cout << "Available commands:\n"
            << ":begin        - begin a transaction (and a new session)\n"
            << ":commit       - commit the transaction of the current session\n"
            << ":abort        - abort the transaction of the current session\n"
            << ":session <no> - switch to the given session\n"
            << ":exit         - exit poseidon_cli" << std::endl;
}

/**
 *
 */
bool process_cmd(graph_db_ptr &gdb, const std::string &cmds) {
  std::string cmd(cmds);
  boost::to_lower(cmd);

  if (cmd == ":begin") {
    // start the session a in new thread
    auto sess = std::make_shared<session>(gdb);
    sess->spawn();
    current_session = ++last_session;
    session_list.insert(std::make_pair(current_session, sess));
    sess->channel().push(msg(msg::BOT));
    spdlog::debug("Session {} created.", current_session);
  } else if (cmd == ":commit") {
    auto it = session_list.find(current_session);
    // commit in the associated thread (session)
    it->second->channel().push(msg(msg::Commit));
    it->second->exit();
    spdlog::debug("Session {} commited.", current_session);
    // remove session from session_list
    session_list.erase(it);
  } else if (cmd == ":abort") {
    auto it = session_list.find(current_session);
    // abort is in the associated thread
    it->second->channel().push(msg(msg::Abort));
    it->second->exit();
    spdlog::debug("Session {} aborted.", current_session);
    // remove session from session_list
    session_list.erase(it);
  } else if (boost::starts_with(cmd, ":session ")) {
    auto session_no = std::stoi(cmd.substr(9, cmd.size()));
    if (session_list.find(session_no) == session_list.end())
      std::cerr << "Invalid session " << session_no << std::endl;
    else {
      std::cout << "Switch to session " << session_no << "..." << std::endl;
      current_session = session_no;
    }
  } else if (cmd == ":help") {
    print_help();
  } else if (cmd == ":exit" || cmd == ":quit") {
    end_sessions();
    return false;
  } else
    std::cout << "Unknown command '" << cmd
              << "' - use :help for a list of available commands." << std::endl;
  return true;
}

void exec_script(graph_db_ptr &gdb, const std::string &cmd) {
  std::ifstream fd(cmd);
  if (!fd.is_open()) {
    std::cerr << "ERROR: cannot open script file.\n";
    return;
  }
  std::string buf;
  while (!fd.eof()) {
    getline(fd, buf);
    trim(buf);
    if (buf.empty())
      continue;

    if (buf[0] == ':') {
      process_cmd(gdb, buf);
    } else {
      auto it = session_list.find(current_session);
      if (it != session_list.end()) {
        spdlog::debug("Execute query in session {}.", current_session);
        it->second->channel().push(msg(msg::ExecPlan, buf));
      } else
        std::cerr << "Cannot execute statement outside a transaction."
                  << std::endl;
    }
  }
  fd.close();
}

/**
 * Run an interactive shell for entering and executing Cypher queries.
 */
void run_shell(graph_db_ptr &gdb) {
  const auto path = "history.txt";
  // Enable the multi-line mode
  linenoise::SetMultiLine(true);

  // Set max length of the history
  linenoise::SetHistoryMaxLen(4);
  // Load history
  linenoise::LoadHistory(path);

  while (true) {
    std::string line;
    auto quit = linenoise::Readline("cypher> ", line);

    if (quit) {
      end_sessions();
      std::cout << "Bye!" << std::endl;
      break;
    }

    trim(line);
    if (line.length() == 0)
      continue;

    if (line[0] == ':') {
      if (!process_cmd(gdb, line)) {
        std::cout << "Bye!" << std::endl;
        break;
      }
    } else {
      auto it = session_list.find(current_session);
      if (it != session_list.end()) {
        spdlog::debug("Execute query in session {}.", current_session);
        it->second->channel().push(msg(msg::ExecPlan, line));
      } else
        std::cerr << "Cannot execute statement outside a transaction."
                  << std::endl;
      // exec_query(gdb, line);
    }

    // Add line to history
    linenoise::AddHistory(line.c_str());

    // Save history
    linenoise::SaveHistory(path);
  }
}

int main(int argc, char **argv) {
  std::string db_name, query_string, cmd_file;
  std::vector<std::string> import_files;
  bool start_shell = false;
  bool exec_ldbc = false;
  char delim_character = ',';

  spdlog::info("Starting poseidon_cli, Version {}",
               POSEIDON_VERSION);

  try {
    options_description desc{"Options"};
    desc.add_options()("help,h", "Help")("verbose,v",
                                         bool_switch()->default_value(false),
                                         "Verbose - show debug output")(
        "db,d", value<std::string>(&db_name)->required(),
        "Database name (required)")(
        "import", value<std::vector<std::string>>()->composing(),
        "Import files in CSV format (either nodes:<node type>:<filename> or "
        "relationships:<filename>")("query,q",
                                    value<std::string>(&query_string),
                                    "Execute the given Cypher query")(
        "exec,e", value<std::string>(&cmd_file),
        "Load and execute the cypher commands from the script")(
        "ldbc", bool_switch()->default_value(false),
        "Execute LDBC benchmark queries")("delimiter",
                                          value<char>(&delim_character),
                                          "Delimiter character for CVS import")(
        "shell,s", bool_switch()->default_value(false),
        "Start the interactive shell");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << "Poseidon Graph Database Version " << POSEIDON_VERSION
                << "\n"
                << desc << '\n';
      return -1;
    }

    notify(vm);

    if (vm.count("import"))
      import_files = vm["import"].as<std::vector<std::string>>();

    if (vm.count("delimiter"))
      delim_character = vm["delimiter"].as<char>();

    if (vm.count("verbose"))
      if (vm["verbose"].as<bool>())
        spdlog::set_level(spdlog::level::debug);

    if (vm.count("shell"))
      start_shell = vm["shell"].as<bool>();

    if (vm.count("ldbc"))
      exec_ldbc = vm["ldbc"].as<bool>();

    if (vm.count("exec"))
      cmd_file = vm["exec"].as<std::string>();

    if (start_shell && !query_string.empty()) {
      std::cout
          << "ERROR: options --shell and --query cannot be used together.\n";
      return -1;
    }
    if (start_shell && exec_ldbc) {
      std::cout
          << "ERROR: options --shell and --ldbc cannot be used together.\n";
      return -1;
    }
    if (start_shell && !cmd_file.empty()) {
      std::cout
          << "ERROR: options --shell and --exec cannot be used together.\n";
      return -1;
    }
    if (!query_string.empty() && exec_ldbc) {
      std::cout
          << "ERROR: options --query and --ldbc cannot be used together.\n";
      return -1;
    }
  } catch (const error &ex) {
    std::cerr << ex.what() << '\n';
    return -1;
  }

#ifdef USE_PMDK
  namespace nvm = pmem::obj;

  nvm::pool<root> pop;
  const auto path = PMEM_PATH + db_name;

  if (access(path.c_str(), F_OK) != 0) {
    pop = nvm::pool<root>::create(path, db_name, POOL_SIZE);
  } else {
    pop = nvm::pool<root>::open(path, db_name);
  }

  auto q = pop.root();
  if (!q->graph) {
    // create a new persistent graph_db object
    nvm::transaction::run(pop, [&] { q->graph = p_make_ptr<graph_db>(); });
  }
  auto &graph = q->graph;
  graph->runtime_initialize();
#else
  auto graph = p_make_ptr<graph_db>(db_name);
#endif

  if (!import_files.empty()) {
    std::cout << "import files..." << std::endl;
    import_csv_files(graph, import_files, delim_character);
    graph->print_mem_usage();
  }

  if (exec_ldbc)
    run_ldbc_queries(graph);

  if (start_shell)
    run_shell(graph);

  if (!query_string.empty()) {
    exec_query(graph, query_string);
  }

  if (!cmd_file.empty()) {
    exec_script(graph, cmd_file);
  }
}
