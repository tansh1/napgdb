#ifndef cypher_hpp_
#define cypher_hpp_

#include <tao/pegtl.hpp>
#include <tao/pegtl/contrib/parse_tree.hpp>

#include "query_compiler.hpp"

/**
 * The grammar for the Cypher query language (a subset of it).
 */
namespace cypher {

using namespace tao::pegtl;

struct node_label : seq<one<':'>, ascii::identifier> {};

struct node_labels : plus<node_label> {};

struct variable : ascii::identifier {};

struct prop_key : ascii::identifier {};

struct escaped
    : if_must<one<'\\'>, one<'\\', '"', '\'', 'a', 'f', 'n', 'r', 't', 'v'>> {};
struct regular : not_range<0, 31> {};
struct character : sor<escaped, regular> {};
struct quoted_string : if_must<one<'"'>, until<one<'"'>, character>> {};

struct integer : plus<ascii::digit> {};

struct prop_val_expression : sor<quoted_string, integer> {};

struct map_literal
    : seq<prop_key, opt<space>, one<':'>, opt<space>, prop_val_expression> {};

struct comma : seq<opt<space>, one<','>, opt<space>> {};

struct properties : seq<one<'{'>, opt<space>, list<map_literal, comma>,
                        opt<space>, one<'}'>> {};

struct node_pattern : seq<one<'('>, opt<variable>, opt<node_labels>, opt<space>,
                          opt<properties>, opt<space>, one<')'>> {};

struct lower_boundary : integer {};
struct upper_boundary : integer {};

struct range_literal : seq<one<'*'>, lower_boundary,
                           opt<seq<TAO_PEGTL_STRING(".."), upper_boundary>>> {};

struct relationship_type : seq<one<':'>, ascii::identifier> {};

struct relationship_types : plus<relationship_type> {};

struct relationship_detail
    : seq<one<'['>, opt<variable>, opt<relationship_types>, opt<range_literal>,
          one<']'>> {};

struct lr_pattern
    : seq<one<'-'>, opt<relationship_detail>, TAO_PEGTL_STRING("->")> {};

struct rl_pattern
    : seq<TAO_PEGTL_STRING("<-"), opt<relationship_detail>, one<'-'>> {};

struct bdir_pattern : seq<TAO_PEGTL_STRING("<-"), opt<relationship_detail>,
                          TAO_PEGTL_STRING("->")> {};

struct ndir_pattern : seq<one<'-'>, opt<relationship_detail>, one<'-'>> {};

struct relationship_pattern
    : sor<bdir_pattern, lr_pattern, rl_pattern, ndir_pattern> {};

struct pattern_element
    : must<node_pattern, star<seq<relationship_pattern, node_pattern>>> {};

struct match_clause : seq<TAO_PEGTL_KEYWORD("MATCH"), space, pattern_element> {
};

struct variable_property : seq<variable, one<'.'>, prop_key> {};

struct return_item : sor<one<'*'>, variable_property, variable> {};

struct return_body : list<return_item, comma> {};

struct return_clause : must<TAO_PEGTL_KEYWORD("RETURN"), space, return_body> {};

struct create_clause
    : must<TAO_PEGTL_KEYWORD("CREATE"), space, pattern_element> {};

// TODO: complete expression definition
struct expression : sor<prop_val_expression, properties> {};

struct assignment : seq<sor<variable_property, variable>, space, one<'='>,
                        space, expression> {};

struct assignment_list : list<assignment, comma> {};

struct set_clause : seq<TAO_PEGTL_KEYWORD("SET"), space, assignment_list> {};

/*
struct delete_clause
    : must<opt<TAO_PEGTL_KEYWORD("DETACH")>, space, TAO_PEGTL_KEYWORD("DELETE"),
space, > {};
*/

struct limit_clause : seq<TAO_PEGTL_KEYWORD("LIMIT"), space, integer> {};

struct match_or_update_clause
    : seq<match_clause, space, opt<seq<set_clause, space>>, return_clause> {};

// TODO: optional add CREATE, SET; make RETURN optional
struct sp_query_clause : sor<match_or_update_clause, create_clause> {};

template <typename Rule> struct my_selector : std::false_type {};
template <> struct my_selector<node_label> : std::true_type {};
template <> struct my_selector<node_pattern> : std::true_type {};
template <> struct my_selector<map_literal> : std::true_type {};
template <> struct my_selector<prop_key> : std::true_type {};
template <> struct my_selector<properties> : std::true_type {};
template <> struct my_selector<prop_val_expression> : std::true_type {};
template <> struct my_selector<relationship_type> : std::true_type {};
template <> struct my_selector<relationship_detail> : std::true_type {};
template <> struct my_selector<lower_boundary> : std::true_type {};
template <> struct my_selector<upper_boundary> : std::true_type {};
template <> struct my_selector<range_literal> : std::true_type {};
template <> struct my_selector<lr_pattern> : std::true_type {};
template <> struct my_selector<rl_pattern> : std::true_type {};
template <> struct my_selector<bdir_pattern> : std::true_type {};
template <> struct my_selector<ndir_pattern> : std::true_type {};
template <> struct my_selector<variable> : std::true_type {};
template <> struct my_selector<assignment> : std::true_type {};
template <> struct my_selector<set_clause> : std::true_type {};
template <> struct my_selector<match_clause> : std::true_type {};
template <> struct my_selector<create_clause> : std::true_type {};
template <> struct my_selector<return_clause> : std::true_type {};
template <> struct my_selector<return_item> : std::true_type {};
template <> struct my_selector<variable_property> : std::true_type {};

template <typename Rule> struct my_control : tao::pegtl::normal<Rule> {
  static const std::string error_message;

  template <typename Input, typename... States>
  static void raise(const Input &in, States &&...) {
    throw tao::pegtl::parse_error(error_message, in);
  }
};

template <typename T>
const std::string my_control<T>::error_message = "parse error";
//   "parse error matching " + tao::pegtl::internal::demangle< T >();

} // namespace cypher

#endif