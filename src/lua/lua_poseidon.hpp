#ifndef lua_poseidon_hpp_
#define lua_poseidon_hpp_

#include "defs.hpp"
#include "nodes.hpp"
#include "qop.hpp"
#include <sol/sol.hpp>

struct lua_node {
  node *node_;
};

struct lua_relationship {
  relationship *rship_;
};

class lua_interp {
public:
  lua_interp(graph_db_ptr &g) : gdb_(g) {
    lua_.open_libraries(sol::lib::base);
    register_lua_functions();
    load_udf_library();
  }

  node::id_t node_id(const lua_node &ln);
  lua_node start_node(const lua_relationship &lr);
  lua_node end_node(const lua_relationship &lr);

  sol::object node_property(const lua_node &n, const std::string &key);
  sol::object rship_property(const lua_relationship &r, const std::string &key);

  void exec_script(const std::string &script);
  void load_script(const std::string &sname);

  void load_udf_library();

  sol::state &state() { return lua_; }

  static void set_udf_library(const std::string &fname) {
    udf_lib_file_ = fname;
  }

private:
  void register_lua_functions();

  graph_db_ptr gdb_;
  sol::state lua_;

  static std::string udf_lib_file_;
};

/**
 * call_lua_procedure represents a query operator for invoking a user-defined
 * procedure implemented in LUA. The operator can act as a root operator (via
 * start) are also as subscriber.
 */
struct call_lua_procedure : public qop {
  call_lua_procedure(graph_db_ptr &gdb, const std::string &proc,
                     const std::vector<std::size_t> &params);
  virtual ~call_lua_procedure() {}

  void dump(std::ostream &os) const;

  void start(graph_db_ptr &gdb);
  void process(graph_db_ptr &gdb, const qr_tuple &v);

  sol::object qr_to_lua(const query_result &qr);
  qr_tuple lua_to_qr(sol::protected_function_result &res);

  std::string proc_name_;
  std::vector<std::size_t>
      params_; // the list of positions (variable) in the input tuple
  lua_interp lua_;
};

#endif