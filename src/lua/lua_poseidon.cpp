#include "lua_poseidon.hpp"
#include <boost/hana.hpp>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>

template <typename T> bool is_check(sol::stack_object something) {
  return something.is<T>();
}

std::string lua_interp::udf_lib_file_ = "";

node::id_t lua_interp::node_id(const lua_node &n) { return n.node_->id(); }

lua_node lua_interp::start_node(const lua_relationship &r) {
  node::id_t nid = r.rship_->from_node_id();
  node &n = gdb_->node_by_id(nid);
  return lua_node{&n};
}

lua_node lua_interp::end_node(const lua_relationship &r) {
  node::id_t nid = r.rship_->to_node_id();
  node &n = gdb_->node_by_id(nid);
  return lua_node{&n};
}

sol::object lua_interp::node_property(const lua_node &n,
                                      const std::string &key) {
  sol::state_view lua(lua_);
  auto ndescr = gdb_->get_node_description(*n.node_);
  auto it = ndescr.properties.find(key);
  if (it != ndescr.properties.end()) {
    auto v = it->second;
    if (v.type() == typeid(int)) {
      return sol::make_object(lua, boost::any_cast<int>(v));
    } else if (v.type() == typeid(double)) {
      return sol::make_object(lua, boost::any_cast<double>(v));
    } else if (v.type() == typeid(bool)) {
      return sol::make_object(lua, boost::any_cast<bool>(v));
    } else if (v.type() == typeid(const char *)) {
      return sol::make_object(lua, boost::any_cast<const char *>(v));
    } else if (v.type() == typeid(const std::string &)) {
      return sol::make_object(lua, boost::any_cast<const std::string &>(v));
    }
  }
  return sol::make_object(lua, sol::lua_nil);
}

void lua_interp::register_lua_functions() {
  spdlog::info("registering lua functions...");
  lua_.set_function("id", &lua_interp::node_id, std::ref(*this));
  lua_.set_function("property", &lua_interp::node_property, std::ref(*this));
  lua_.set_function("start_node", &lua_interp::start_node, std::ref(*this));
  lua_.set_function("end_node", &lua_interp::end_node, std::ref(*this));
}

void lua_interp::exec_script(const std::string &scr) {
  spdlog::info("executing lua code {}", scr);
  lua_.script(scr);
}

void lua_interp::load_script(const std::string &sname) {
  spdlog::info("loading lua script {}", sname);
  lua_.script_file(sname);
}

void lua_interp::load_udf_library() {
  const char *fname = getenv("POSEIDON_UDF_LIBRARY");
  if (fname == NULL)
    fname = udf_lib_file_.c_str();
  if (access(fname, R_OK) == 0)
    load_script(fname);
  else
    spdlog::info("Cannot load LUA UDF script {}", fname);
}

call_lua_procedure::call_lua_procedure(graph_db_ptr &gdb,
                                       const std::string &proc,
                                       const std::vector<size_t> &params)
    : proc_name_(proc), params_(params), lua_(gdb) {}

void call_lua_procedure::dump(std::ostream &os) const {
  os << "call_lua_procedure(" << proc_name_ << "[ ";
  for (auto &v : params_) {
    os << v << " ";
  }
  os << "])=>";
  if (subscriber_)
    subscriber_->dump(os);
}

void call_lua_procedure::start(graph_db_ptr &gdb) {
  // TODO: handle params?
  sol::protected_function f = lua_.state()[proc_name_];
  sol::protected_function_result res = f();
  if (res.valid()) {
    auto qt = lua_to_qr(res);
    consume_(gdb, qt);
  }
}

void call_lua_procedure::process(graph_db_ptr &gdb, const qr_tuple &v) {
  std::vector<sol::object> args(params_.size());
  for (auto i = 0u; i < params_.size(); i++) {
    // create sol::object from v
    args[i] = qr_to_lua(v[params_[i]]);
  }

  sol::protected_function f = lua_.state()[proc_name_];
  sol::protected_function_result res = f(sol::as_args(args));
  if (res.valid()) {
    auto qt = lua_to_qr(res);
    auto v2 = concat(v, qt);
    consume_(gdb, v2);
  }
}

qr_tuple call_lua_procedure::lua_to_qr(sol::protected_function_result &res) {
  qr_tuple qt(res.return_count());
  for (auto i = 0; i < res.return_count(); i++) {
    query_result qr;
    switch (res.get_type(i)) {
    case sol::type::number: {
      // LUA doesn't distinguish between int and double. As a workaround we
      // extract both and compare them. If the two values are equal then we
      // return an int value, otherwise a double value.
      int v = res.get<int>(i);
      double d = res.get<double>(i);
      if (d == v)
        qr = v;
      else
        qr = d;
    } break;
    case sol::type::string: {
      std::string s = res.get<std::string>(i);
      qr = s;
    } break;
    case sol::type::userdata: {
      sol::userdata d = res.get<sol::userdata>(i);
      if (d.is<lua_node>()) {
        auto ln = d.as<lua_node>();
        qr = ln.node_;
      } else if (d.is<lua_relationship>()) {
        auto lr = d.as<lua_relationship>();
        qr = lr.rship_;
      }
    } break;
    default:
      // shouldn't happen
      break;
    }
    qt[i] = qr;
  }
  return qt;
}

sol::object call_lua_procedure::qr_to_lua(const query_result &qr) {
  sol::state_view lua(lua_.state());
  auto my_visitor = boost::hana::overload(
      [&](node *n) { return sol::make_object(lua, lua_node{n}); },
      [&](relationship *r) {
        return sol::make_object(lua, lua_relationship{r});
      },
      [&](int i) { return sol::make_object(lua, i); },
      [&](double d) { return sol::make_object(lua, d); },
      [&](const std::string &s) { return sol::make_object(lua, s); });
  return boost::apply_visitor(my_visitor, qr);
}
