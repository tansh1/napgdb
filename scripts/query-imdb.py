# Python example for using Poseidon graph database
# Open an existing database and execute a query

# NOTE: this works only if PMem is used and the database 'imdb'
# was created before (e.g. using the create-imdb.py script).
import poseidon

# Create a new database
#
graph = poseidon.graph_db()
graph.open("imdb")

# we obtain the dictionary code for the movie title
c = graph.dict_code("Inception (2010)")
print("dict_code for 'Inception' = ", c)

# print all actors of the movie Inception
q = poseidon.query(graph) \
    .nodes_where("Movie", "title", lambda p: p.dict_eq(c)) \
    .to_relationships("PLAYED_IN") \
    .from_node("Actor") \
    .print()

q.start()
