:begin
CREATE (a:Actor { name: "Leonardo DiCaprio" })
CREATE (a:Actor { name: "Matthew McConaughey" })
CREATE (m:Movie { title: "Inception" })
CREATE (m:Movie { title: "Interstellar" })
:commit
:begin
MATCH (a:Actor) RETURN a
:commit
