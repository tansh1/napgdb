# Python example for using Poseidon graph database
# Create a database, import IMDB data from CSV files and execute a query

import poseidon
import sys
sys.path.append('.')

# Create a new database
#
graph = poseidon.graph_db()
graph.create("imdb")

# Import data
#
m = graph.mapping()  # m is a mapping table for import of relationships
n = graph.import_nodes("Movie", "../imdb-data/movies.csv", m)
print(n, "movies imported.")
n = graph.import_nodes("Actor", "../imdb-data/actors.csv", m)
print(n, "actors imported.")
n = graph.import_relationships("../imdb-data/roles.csv", m)
print(n, "roles imported.")
m.reset()  # we don't need m anymore, let's free memory

# we obtain the dictionary code for the movie title
c = graph.dict_code("Inception (2010)")
print("dict_code for 'Inception' = ", c)

# print all actors of the movie Inception
q = poseidon.query(graph) \
    .nodes_where("Movie", "title", lambda p: p.dict_eq(c)) \
    .to_relationships("PLAYED_IN") \
    .from_node("Actor") \
    .print()

# q = poseidon.query(graph) \
#     .all_nodes() \
#     .from_relationships("PLAYED_IN") \
#     .to_node("Movie") \
#     .print()

q.dump()
q.start()
